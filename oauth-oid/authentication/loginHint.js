import bcrypt from 'bcrypt';

import config from '../../../../config/server_config';

import { UnknownUserIDError, InvalidUserCodeError, InvalidUserIdOrUserCodeError } from '../../errors';

const { query } = require('nact');

export default async function (login_hint, user_code, ctx) {
  const collectionItemActor = ctx.children.get('item');

  const user = await query(collectionItemActor, { type: 'user', search: { email: login_hint } }, global.actor_timeout);

  if (!user) {
    throw config.secure_auth ? new InvalidUserIdOrUserCodeError('Wrong user code or password') : new UnknownUserIDError('No user with that email');
  }

  const authenticator = await query(collectionItemActor, { type: 'associate_session', search: { user_code } }, global.actor_timeout);

  if (!authenticator) {
    const valid = bcrypt.compareSync(user_code, user.crypto_password);
    if (!valid) {
      throw config.secure_auth ? new InvalidUserIdOrUserCodeError('Wrong user code or password') : new InvalidUserCodeError('Invalid password or User Code');
    }
  }

  return user;
}
