import authorizationHandler from './handlers/authorizationHandler';
import tokenHandler from './handlers/tokenHandler';
import introspectHandler from './handlers/introspectHandler';
import revokeHandler from './handlers/revokeHandler';

module.exports = {

  authorizationHandler,
  tokenHandler,
  introspectHandler,
  revokeHandler,

};
